import os
import sys
from dotenv import load_dotenv, find_dotenv

def envs_init():

    sys.path.append('../..')
    _ = load_dotenv(find_dotenv(raise_error_if_not_found=True), verbose=True)  # read local .env file

    langchain_tracing_v2 = os.environ["LANGCHAIN_TRACING_V2"]
    langchain_endpoint = os.environ["LANGCHAIN_ENDPOINT"]
    langchain_api_key = os.environ["LANGCHAIN_API_KEY"]
    langchain_project = os.environ["LANGCHAIN_PROJECT"]
    openai_api_key = os.environ["OPENAI_API_KEY"]
    anthropic_api_key = os.environ["ANTHROPIC_API_KEY"]
    tavily_api_key = os.environ["TAVILY_API_KEY"]
    serpapi_api_key = os.environ["SERPAPI_API_KEY"]

    print("Environment variables loaded successfully")


# Reset or remove the environment variable
# if "LANGCHAIN_TRACING_V2" in os.environ:
#     del os.environ["LANGCHAIN_TRACING_V2"]
# if "LANGCHAIN_ENDPOINT" in os.environ:
#     del os.environ["LANGCHAIN_ENDPOINT"]
# if "LANGCHAIN_API_KEY" in os.environ:
#     del os.environ["LANGCHAIN_API_KEY"]
# if "LANGCHAIN_PROJECT" in os.environ:
    # del os.environ["LANGCHAIN_PROJECT"]