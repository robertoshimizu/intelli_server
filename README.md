# intelli_server

## Installation

Install the LangChain CLI if you haven't yet

```bash
pip install -U langchain-cli
```

## Adding packages

```bash
# adding packages from 
# https://github.com/langchain-ai/langchain/tree/master/templates
langchain app add $PROJECT_NAME

# adding custom GitHub repo packages
langchain app add --repo $OWNER/$REPO
# or with whole git string (supports other git providers):
# langchain app add git+https://github.com/hwchase17/chain-of-verification

# with a custom api mount point (defaults to `/{package_name}`)
langchain app add $PROJECT_NAME --api_path=/my/custom/path/rag
```

Note: you remove packages by their api path

```bash
langchain app remove my/custom/path/rag
```

## Setup LangSmith (Optional)
LangSmith will help us trace, monitor and debug LangChain applications. 
LangSmith is currently in private beta, you can sign up [here](https://smith.langchain.com/). 
If you don't have access, you can skip this section


```shell
export LANGCHAIN_TRACING_V2=true
export LANGCHAIN_API_KEY=<your-api-key>
export LANGCHAIN_PROJECT=<your-project>  # if not specified, defaults to "default"
```

## Launch LangServe

```bash
langchain serve
```

## Running in Docker

This project folder includes a Dockerfile that allows you to easily build and host your LangServe app.

### Building the Image

To build the image, you simply:

```shell
docker build . -t my-langserve-app
```

If you tag your image with something other than `my-langserve-app`,
note it for use in the next step.

### Running the Image Locally

To run the image, you'll need to include any environment variables
necessary for your application.

In the below example, we inject the `OPENAI_API_KEY` environment
variable with the value set in my local environment
(`$OPENAI_API_KEY`)

We also expose port 8080 with the `-p 8080:8080` option.

```shell
docker run -e OPENAI_API_KEY=$OPENAI_API_KEY -p 8080:8080 my-langserve-app
```



### Basic Chat model

```python
model = ChatOpenAI()
```
From the client, this is the json that needs to be in the Body
```
{
    "input": [
        {
            "content": "que eh Jack Sparrow?",
            "type": "human"
        }
    ],
    "config": {}
}
```

#### Response

- Invoke
```
{
  "output": {
    "content": "Jack Sparrow es un personaje ficticio y el protagonista de la serie de películas de \"Piratas del Caribe\". Es interpretado por el actor Johnny Depp. Sparrow es un carismático y excéntrico pirata, conocido por su estilo de vestir extravagante, su andar peculiar y su ingenio para salir de situaciones peligrosas. A lo largo de las películas, se embarca en diversas aventuras en busca de tesoros y enfrentándose a enemigos como el Capitán Barbossa y Davy Jones.",
    "additional_kwargs": {},
    "type": "ai",
    "example": false
  },
  "callback_events": [],
  "metadata": {
    "run_id": "1ba06d38-a562-480d-aac0-b3fc56e5c740"
  }
}
```

- Stream

```
event: metadata
data: {"run_id": "84694db6-8c55-4805-8444-071592803d2f"}
...
event: data
data: {"content":" fict","additional_kwargs":{},"type":"AIMessageChunk","example":false}
...
event: data
data: {"content":".","additional_kwargs":{},"type":"AIMessageChunk","example":false}

event: data
data: {"content":"","additional_kwargs":{},"type":"AIMessageChunk","example":false}

event: end
```

#### Same applies to anthropic

payload
```
'{
  "input": [{
              "content": "Who is Mick Jagger?",
              "type": "human"
           }],
  "config": {},
  "kwargs": {}
}'
```
Output:
invoke:
```
{
  "output": {
    "content": " Mick Jagger is a famous British singer and songwriter. Some key information about him:\n\n- He is best known as the lead singer and one of the founding members of The Rolling Stones, one of the most popular and influential rock bands of all time. \n\n- With The Rolling Stones since 1962, Jagger has helped create major rock hits like \"(I Can't Get No) Satisfaction\", \"Get Off of My Cloud\", \"Wild Horses\", \"Angie\", and many more.\n\n- He is regarded as one of the greatest and most dynamic frontmen in rock and roll history thanks to his energetic stage presence and distinctive voice.\n\n- In addition to his work with The Rolling Stones, Jagger has also had a successful solo career with hits like \"Just Another Night\" and \"Dancing in the Street.\"\n\n- Some of Jagger's notable accomplishments include being inducted into the Rock and Roll Hall of Fame (with The Rolling Stones) in 1989, winning multiple Grammy Awards, a Golden Globe for his acting, and knighthood from the British monarchy in 2003.\n\n- Now 79 years old, Jagger is still actively touring and performing with The Rolling Stones, showc",
    "additional_kwargs": {},
    "type": "ai",
    "example": false
  },
  "callback_events": [],
  "metadata": {
    "run_id": "151e7fa3-e9f4-4144-a9e0-184c97f079b6"
  }
}```

### Output Using Runnables

```
{
  content: '{"results":[{"content":"Fico feliz em poder ajudar! Se tiver mais alguma pergunta ou precisar de mais informações, é só me avisar. Estou à disposição!","type":"ai"}]}',
  additional_kwargs: {},
  type: 'ai',
  example: false
}```